package com.capgemini.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.capgemini.dao.QueryDao;
import com.capgemini.model.criteriaobject.EmployeeSearchCriteria;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.model.enumerator.EmployeeRole;

@Repository
@Profile("jpql")
public class JpqlQueryDaoImpl implements QueryDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<EmployeeEntity> findEmployeesByCriteria(EmployeeSearchCriteria criteria) {
		Long projectId = criteria.getProjectId();
		EmployeeRole projectRole = criteria.getRole();
		Date projectFirstDate = criteria.getFirstDate();
		Date projectSecondDate = criteria.getSecondDate();
		String project = projectId == null ? " IS NOT NULL " : " = :projectId ";
		String role = projectRole == null ? " IS NOT NULL " : " = :role ";
		String firstDate = setFirstDate(projectFirstDate);
		String secondDate = setSecondDate(projectSecondDate);
		TypedQuery<EmployeeEntity> query = em
				.createQuery("SELECT DISTINCT e.employee FROM EmployeeInProjectEntity e WHERE e.project.id" + project
						+ "AND e.role" + role + "AND (" + firstDate + " OR" + secondDate + ")", EmployeeEntity.class);
		setParameters(projectId, projectRole, projectFirstDate, projectSecondDate, query);
		return query.getResultList();
	}

	@Override
	public int countProjectWithOver10Employees(Date date) {
		return this.findProjectWithOver10Employees(date).size();
	}

	@Override
	public List<ProjectEntity> findProjectWithOver10Employees(Date date) {
		TypedQuery<ProjectEntity> query = em
				.createQuery("SELECT DISTINCT e.project " + "FROM EmployeeInProjectEntity e "
						+ "WHERE ((e.endDate IS NOT NULL AND :date BETWEEN e.startDate AND e.endDate) "
						+ "OR (e.endDate IS NULL AND :date BETWEEN e.startDate AND current_date())) GROUP BY e.project "
						+ "HAVING COUNT(e) > 10", ProjectEntity.class);
		List<ProjectEntity> resultList = query.setParameter("date", date).getResultList();
		return resultList;
	}

	private String setSecondDate(Date date) {
		String secondDate = date == null
				? " ((e.endDate IS NULL AND current_date() BETWEEN e.startDate AND current_date()) "
						+ "OR (e.endDate IS NOT NULL AND current_date() BETWEEN e.startDate AND e.endDate))"
				: " ((e.endDate IS NULL AND :secondDate BETWEEN e.startDate AND current_date()) "
						+ "OR (e.endDate IS NOT NULL AND :secondDate BETWEEN e.startDate AND e.endDate))";
		return secondDate;
	}

	private String setFirstDate(Date date) {
		String firstDate = date == null ? " e.startDate IS NOT NULL "
				: " ((e.endDate IS NULL AND :firstDate BETWEEN e.startDate AND current_date()) "
						+ "OR (e.endDate IS NOT NULL AND :firstDate BETWEEN e.startDate AND e.endDate))";
		return firstDate;
	}

	private void setParameters(Long projectId, EmployeeRole projectRole, Date projectFirstDate, Date projectSecondDate,
			TypedQuery<EmployeeEntity> query) {
		if (projectId != null) {
			query.setParameter("projectId", projectId);
		}
		if (projectRole != null) {
			query.setParameter("role", projectRole);
		}
		if (projectFirstDate != null) {
			query.setParameter("firstDate", projectFirstDate);
		}
		if (projectSecondDate != null) {
			query.setParameter("secondDate", projectSecondDate);
		}
	}
}
