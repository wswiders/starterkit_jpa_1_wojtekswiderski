package com.capgemini.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.capgemini.dao.QueryDao;
import com.capgemini.model.criteriaobject.EmployeeSearchCriteria;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.model.enumerator.EmployeeRole;
import com.capgemini.model.mapper.EmployeeResoultMapper;
import com.capgemini.model.mapper.ProjectResoultMapper;

@Repository
@Profile("jdbc")
public class JdbcTemplateQueryDaoImpl implements QueryDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public int countProjectWithOver10Employees(Date date) {
		return this.findProjectWithOver10Employees(date).size();
	}

	@Override
	public List<ProjectEntity> findProjectWithOver10Employees(Date date) {
		String sql = "SELECT DISTINCT * FROM project p " 
				+ "JOIN employee_in_project e ON e.project_id = p.id "
				+ "WHERE ((e.end_date IS NOT NULL AND :date BETWEEN e.start_date AND e.end_date) "
				+ "OR (e.end_date IS NULL AND :date BETWEEN e.start_date AND NOW())) " 
				+ "GROUP BY e.project_id "
				+ "HAVING COUNT(*) > 10";
		MapSqlParameterSource param = new MapSqlParameterSource("date", date);
		List<ProjectEntity> resoults = jdbcTemplate.query(sql, param, new ProjectResoultMapper());
		return resoults;
	}

	@Override
	public List<EmployeeEntity> findEmployeesByCriteria(EmployeeSearchCriteria criteria) {
		Long projectId = criteria.getProjectId();
		EmployeeRole projectRole = criteria.getRole();
		Date projectFirstDate = criteria.getFirstDate();
		Date projectSecondDate = criteria.getSecondDate();
		String project = projectId == null ? " IS NOT NULL " : " = :projectId ";
		String role = projectRole == null ? " IS NOT NULL " : " = :role ";
		String firstDate = setFirstDate(projectFirstDate);
		String secondDate = setSecondDate(projectSecondDate);
		String sql = "SELECT DISTINCT * FROM employee em " 
				+ "JOIN employee_in_project e ON e.employee_id = em.id "
				+ "WHERE e.project_id" + project 
				+ "AND e.role" + role 
				+ "AND (" + firstDate + " OR" + secondDate + ")";
		MapSqlParameterSource param = new MapSqlParameterSource();
		setParameters(projectId, projectRole, projectFirstDate, projectSecondDate, param);
		List<EmployeeEntity> resoults = jdbcTemplate.query(sql, param, new EmployeeResoultMapper());
		return resoults;
	}

	private void setParameters(Long projectId, EmployeeRole projectRole, Date projectFirstDate, Date projectSecondDate,
			MapSqlParameterSource param) {
		if (projectId != null) {
			param.addValue("projectId", projectId);
		}
		if (projectRole != null) {
			param.addValue("role", projectRole);
		}
		if (projectFirstDate != null) {
			param.addValue("firstDate", projectFirstDate);
		}
		if (projectSecondDate != null) {
			param.addValue("secondDate", projectSecondDate);
		}
	}

	private String setSecondDate(Date date) {
		String secondDate = date == null
				? " ((e.end_date IS NULL AND NOW() BETWEEN e.start_date AND NOW()) "
						+ "OR (e.end_date IS NOT NULL AND NOW() BETWEEN e.start_date AND e.end_date))"
				: " ((e.end_date IS NULL AND :secondDate BETWEEN e.start_date AND NOW()) "
						+ "OR (e.end_date IS NOT NULL AND :secondDate BETWEEN e.start_date AND e.end_date))";
		return secondDate;
	}

	private String setFirstDate(Date date) {
		String firstDate = date == null ? " e.start_date IS NOT NULL "
				: " ((e.end_date IS NULL AND :firstDate BETWEEN e.start_date AND NOW()) "
						+ "OR (e.end_date IS NOT NULL AND :firstDate BETWEEN e.start_date AND e.end_date))";
		return firstDate;
	}
}
