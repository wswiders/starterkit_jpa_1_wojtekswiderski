package com.capgemini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capgemini.model.entity.ProjectEntity;

public interface JpaProjectDao extends JpaRepository<ProjectEntity, Long> {
	
}
