package com.capgemini.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.capgemini.model.entity.EmployeeEntity;

public interface JpaEmployeeDao extends JpaRepository<EmployeeEntity, Long> {

	public List<EmployeeEntity> findByNameContainingAndSurnameContaining(String name, String surname);

	@Query("SELECT e FROM EmployeeEntity e WHERE e.department.id = :departmentId")
	public List<EmployeeEntity> findByDepartmentId(@Param("departmentId") Long departmentId);
}
