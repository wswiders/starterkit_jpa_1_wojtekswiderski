package com.capgemini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capgemini.model.entity.DepartmentEntity;

public interface JpaDepartmentDao extends JpaRepository<DepartmentEntity, Long> {

}
