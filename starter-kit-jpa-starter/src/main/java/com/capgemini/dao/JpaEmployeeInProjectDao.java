package com.capgemini.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.capgemini.model.entity.EmployeeInProjectEntity;

public interface JpaEmployeeInProjectDao extends JpaRepository<EmployeeInProjectEntity, Long> {

	@Query("SELECT eip FROM EmployeeInProjectEntity eip WHERE eip.employee.id = :id AND eip.endDate IS NULL")
	public List<EmployeeInProjectEntity> findActualAssignmentsForEmployee(@Param("id") Long employeeId);

	@Query("SELECT eip FROM EmployeeInProjectEntity eip WHERE eip.project.id = :id AND eip.endDate IS NULL")
	public List<EmployeeInProjectEntity> findActiveAssignmentsWithUnfinishedProjectsByProjectId(@Param("id") Long projectId);

	@Query("SELECT eip FROM EmployeeInProjectEntity eip WHERE eip.employee.id = :employeeId AND eip.project.id = :projectId AND eip.endDate IS NULL")
	public EmployeeInProjectEntity findAssignmentByEmployeeIdAndProjectIdWithNullEndDate(@Param("employeeId") Long employeeId,
			@Param("projectId") Long projectId);
	
	@Query("SELECT eip FROM EmployeeInProjectEntity eip WHERE eip.project.id = :id")
	public List<EmployeeInProjectEntity> findAllAssignmentsByProjectID(@Param("id") Long projectId);
}
