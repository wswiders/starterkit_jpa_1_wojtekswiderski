package com.capgemini.exception;

public class WrongQueryException extends Exception {

	private static final long serialVersionUID = 1L;

	public WrongQueryException(String msg) {
		super(msg);
	}
}
