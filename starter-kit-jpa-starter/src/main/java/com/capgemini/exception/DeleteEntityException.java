package com.capgemini.exception;

public class DeleteEntityException extends Exception {

	private static final long serialVersionUID = 1L;

	public DeleteEntityException(String msg) {
		super(msg);
	}
}
