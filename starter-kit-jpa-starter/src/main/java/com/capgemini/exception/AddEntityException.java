package com.capgemini.exception;

public class AddEntityException extends Exception {

	private static final long serialVersionUID = 1L;

	public AddEntityException(String msg) {
		super(msg);
	}
}
