package com.capgemini.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.capgemini.model.enumerator.ProjectType;

@Entity
@Table(name = "project")
public class ProjectEntity extends AbstractEntity {

	@Column(length = 45, nullable = false, unique = true)
	private String name;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private ProjectType type;

	@OneToMany(mappedBy = "project", orphanRemoval = true)
	private List<EmployeeInProjectEntity> employeeInProject;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProjectType getType() {
		return type;
	}

	public void setType(ProjectType type) {
		this.type = type;
	}

	public List<EmployeeInProjectEntity> getEmployeeInProject() {
		return employeeInProject;
	}
}
