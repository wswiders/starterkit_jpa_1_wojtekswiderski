package com.capgemini.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class EmployeeEntity extends AbstractEntity {

	@Column(length = 45, nullable = false)
	private String name;

	@Column(length = 45, nullable = false)
	private String surname;

	@Column(length = 11)
	private String pesel;

	@Column(name = "birth_date", nullable = false)
	private Date birthDate;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private DepartmentEntity department;

	@Embedded
	@AttributeOverrides({ 
		@AttributeOverride(name = "email", column = @Column(name = "email", length = 100)),
		@AttributeOverride(name = "phone", column = @Column(name = "phone", length = 20)),
		@AttributeOverride(name = "homePhone", column = @Column(name = "home_phone", length = 20))
	})
	private ContactEntity contact;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeInProjectEntity> employeeInProject;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public DepartmentEntity getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}

	public ContactEntity getContact() {
		return contact;
	}

	public void setContact(ContactEntity contact) {
		this.contact = contact;
	}

	public List<EmployeeInProjectEntity> getEmployeeInProject() {
		return employeeInProject;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
}
