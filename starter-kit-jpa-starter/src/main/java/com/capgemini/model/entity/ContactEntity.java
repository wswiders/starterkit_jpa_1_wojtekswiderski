package com.capgemini.model.entity;

import javax.persistence.Embeddable;

@Embeddable
public class ContactEntity{

	private String email;
	
	private String phone;
	
	private String homePhone;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
}
