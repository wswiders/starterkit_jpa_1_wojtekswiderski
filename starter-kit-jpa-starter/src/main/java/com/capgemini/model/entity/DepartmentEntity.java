package com.capgemini.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "department")
public class DepartmentEntity extends AbstractEntity {

	@Column(length = 45, nullable = false, unique = true)
	private String name;

	@Embedded
	@AttributeOverrides({ 
		@AttributeOverride(name = "email", column = @Column(name = "email", length = 100)),
		@AttributeOverride(name = "phone", column = @Column(name = "phone", length = 20)),
		@AttributeOverride(name = "homePhone", column = @Column(name = "home_phone", length = 20))
	})
	private ContactEntity contact;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ContactEntity getContact() {
		return contact;
	}

	public void setContact(ContactEntity contact) {
		this.contact = contact;
	}
}
