package com.capgemini.model.enumerator;

public enum EmployeeRole {

	PL, TCD, FCD, DEV
}
