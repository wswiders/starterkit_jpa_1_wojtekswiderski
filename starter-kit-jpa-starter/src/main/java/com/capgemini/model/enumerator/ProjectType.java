package com.capgemini.model.enumerator;

public enum ProjectType {

	EXTERNAL, INTERNAL
}
