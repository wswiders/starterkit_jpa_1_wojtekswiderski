package com.capgemini.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.model.enumerator.ProjectType;

public class ProjectResoultMapper implements RowMapper<ProjectEntity>{

	@Override
	public ProjectEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProjectEntity project = new ProjectEntity();
		project.setId(rs.getLong("id"));
		project.setVersion(rs.getLong("version"));
		project.setCreationDate(rs.getDate("creation_date"));
		project.setUpdateDate(rs.getDate("last_update"));
		project.setName(rs.getString("name"));
		project.setType(ProjectType.valueOf(rs.getString("type")));
		return project;
	}
}
