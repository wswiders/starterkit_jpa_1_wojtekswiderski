package com.capgemini.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.capgemini.model.entity.DepartmentEntity;
import com.capgemini.model.entity.EmployeeEntity;

public class EmployeeResoultMapper implements RowMapper<EmployeeEntity>{

	@Override
	public EmployeeEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmployeeEntity employee = new EmployeeEntity();
		employee.setId(rs.getLong("id"));
		employee.setVersion(rs.getLong("version"));
		employee.setCreationDate(rs.getDate("creation_date"));
		employee.setUpdateDate(rs.getDate("last_update"));
		employee.setName(rs.getString("name"));
		employee.setSurname(rs.getString("surname"));
		employee.setPesel(rs.getString("pesel"));
		employee.setBirthDate(rs.getDate("birth_date"));
		employee.setDepartment(rs.getObject("department_id", DepartmentEntity.class));
		employee.getContact().setEmail(rs.getString("email"));
		employee.getContact().setHomePhone(rs.getString("home_phone"));
		employee.getContact().setPhone(rs.getString("phone"));
		return employee;
	}

}
