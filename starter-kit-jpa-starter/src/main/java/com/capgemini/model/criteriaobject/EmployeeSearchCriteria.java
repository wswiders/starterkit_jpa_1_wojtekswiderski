package com.capgemini.model.criteriaobject;

import java.util.Date;

import com.capgemini.model.enumerator.EmployeeRole;

public class EmployeeSearchCriteria {

	private Long projectId;
	private EmployeeRole role;
	private Date firstDate;
	private Date secondDate;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public EmployeeRole getRole() {
		return role;
	}

	public void setRole(EmployeeRole role) {
		this.role = role;
	}

	public Date getFirstDate() {
		return firstDate;
	}

	public void setFirstDate(Date firstDate) {
		this.firstDate = firstDate;
	}

	public Date getSecondDate() {
		return secondDate;
	}

	public void setSecondDate(Date secondDate) {
		this.secondDate = secondDate;
	}
}
