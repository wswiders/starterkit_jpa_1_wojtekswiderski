package com.capgemini.service;

import com.capgemini.exception.AddEntityException;
import com.capgemini.model.entity.EmployeeInProjectEntity;

public interface ProjectManageService {

	public boolean isDeletePossible(Long id);

	public void isAddingAssignmentPossible(EmployeeInProjectEntity assignment) throws AddEntityException;

}
