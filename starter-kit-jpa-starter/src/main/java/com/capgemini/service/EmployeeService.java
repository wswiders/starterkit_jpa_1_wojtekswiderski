package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.DeleteEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.EmployeeEntity;

public interface EmployeeService {

	public List<EmployeeEntity> findAll();

	public EmployeeEntity findById(Long employeeId);

	public void save(EmployeeEntity employee);

	public void delete(Long employeeId) throws DeleteEntityException, WrongQueryException;

	public List<EmployeeEntity> findByNameAndSurname(String name, String surname);

	public List<EmployeeEntity> findByDepartment(Long employeeId);

	public void updateData(Long employeeId, EmployeeEntity updatedEntity) throws WrongQueryException;

	public void setNullDepartment(Long employeeId);
}
