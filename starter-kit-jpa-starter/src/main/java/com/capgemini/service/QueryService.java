package com.capgemini.service;

import java.util.Date;
import java.util.List;

import com.capgemini.model.criteriaobject.EmployeeSearchCriteria;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.ProjectEntity;

public interface QueryService {

	public List<EmployeeEntity> findEmployeesByCriteria(EmployeeSearchCriteria criteria);
	
	public int countProjectWithOver10Employees(Date date);

	public List<ProjectEntity> findProjectWithOver10Employees(Date date);
}
