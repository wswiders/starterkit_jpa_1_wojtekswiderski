package com.capgemini.service;

import com.capgemini.model.entity.DepartmentEntity;

public interface DepartmentService {

	public DepartmentEntity findById(Long id);
}
