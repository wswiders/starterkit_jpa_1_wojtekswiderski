package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.DeleteEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.ProjectEntity;

public interface ProjectService {

	public List<ProjectEntity> findAll();

	public ProjectEntity findById(Long id);

	public void save(ProjectEntity project);

	public void delete(Long id) throws DeleteEntityException, WrongQueryException;

	public void updateData(Long id, ProjectEntity updatedProject) throws WrongQueryException;
}
