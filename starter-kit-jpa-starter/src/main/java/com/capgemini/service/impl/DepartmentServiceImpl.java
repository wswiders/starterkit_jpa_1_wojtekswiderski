package com.capgemini.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.JpaDepartmentDao;
import com.capgemini.model.entity.DepartmentEntity;
import com.capgemini.service.DepartmentService;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private JpaDepartmentDao departmentDao;

	@Override
	public DepartmentEntity findById(Long id) {
		return departmentDao.findOne(id);
	}
}
