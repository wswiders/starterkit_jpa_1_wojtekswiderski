package com.capgemini.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.QueryDao;
import com.capgemini.model.criteriaobject.EmployeeSearchCriteria;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.service.QueryService;

@Service
@Transactional
public class QueryServiceImpl implements QueryService {

	@Autowired
	private QueryDao queryDao;
	
	@Override
	public List<EmployeeEntity> findEmployeesByCriteria(EmployeeSearchCriteria criteria) {
		return queryDao.findEmployeesByCriteria(criteria);
	}
	
	@Override
	public int countProjectWithOver10Employees(Date date) {
		return queryDao.countProjectWithOver10Employees(date);
	}

	@Override
	public List<ProjectEntity> findProjectWithOver10Employees(Date date) {
		return queryDao.findProjectWithOver10Employees(date);
	}
}
