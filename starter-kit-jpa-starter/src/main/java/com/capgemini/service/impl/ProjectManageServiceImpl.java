package com.capgemini.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.exception.AddEntityException;
import com.capgemini.model.entity.EmployeeInProjectEntity;
import com.capgemini.model.enumerator.EmployeeRole;
import com.capgemini.service.EmployeeInProjectService;
import com.capgemini.service.ProjectManageService;
import com.capgemini.service.ProjectService;

@Service
@Transactional
public class ProjectManageServiceImpl implements ProjectManageService {

	@Autowired
	private EmployeeInProjectService employeeInProjectService;

	@Autowired
	private ProjectService projectService;

	@Override
	public boolean isDeletePossible(Long projectId) {
		List<EmployeeInProjectEntity> activeAssignments = employeeInProjectService.findActiveAssignmentInProject(projectId);
		if (activeAssignments.size() == 0) {
			return true;
		}
		return false;
	}

	@Override
	public void isAddingAssignmentPossible(EmployeeInProjectEntity assignment) throws AddEntityException {
		Long projectId = assignment.getProject().getId();
		Long employeeId = assignment.getEmployee().getId();
		Date startDate = assignment.getStartDate();
		Date endDate = assignment.getEndDate();
		EmployeeRole role = assignment.getRole();
		BigDecimal salary = assignment.getSalary();
		if (role == EmployeeRole.PL) {
			isManagerInProject(projectId);
		}
		isEmployeeInProject(projectId, employeeId);
		isStartDateCorrect(projectId, startDate);
		isEndDateCorrect(startDate, endDate);
		isSalaryCorrect(salary);
	}

	private void isManagerInProject(Long projectId) throws AddEntityException {
		if (projectId != null) {
			List<EmployeeInProjectEntity> assignments = employeeInProjectService.findActiveAssignmentInProject(projectId);
			for (EmployeeInProjectEntity assignment : assignments) {
				if (assignment.getRole() == EmployeeRole.PL) {
					throw new AddEntityException("In project can be only one PL.");
				}
			}
		}
	}

	private void isEmployeeInProject(Long projectId, Long employeeId) throws AddEntityException {
		if (projectId != null & employeeId != null) {
			List<EmployeeInProjectEntity> assignments = employeeInProjectService.findActiveAssignmentInProject(projectId);
			for (EmployeeInProjectEntity assignment : assignments) {
				if (assignment.getEmployee().getId() == employeeId) {
					throw new AddEntityException("Employee already had role in project.");
				}
			}
		}
	}

	private void isStartDateCorrect(Long projectId, Date startDate) throws AddEntityException {
		if (projectId != null & startDate != null) {
			Date projectStartDate = projectService.findById(projectId).getCreationDate();
			if (startDate.compareTo(projectStartDate) <= 0) {
				throw new AddEntityException("Project start date must be after project creation date.");
			}
		}
	}

	private void isEndDateCorrect(Date startDate, Date endDate) throws AddEntityException {
		if (startDate != null & endDate != null) {
			if (endDate.compareTo(startDate) <= 0) {
				throw new AddEntityException("Project end date must be after project start date.");
			}
		}
	}

	private void isSalaryCorrect(BigDecimal salary) throws AddEntityException {
		if (salary != null) {
			if (salary.compareTo(BigDecimal.ZERO) < 0) {
				throw new AddEntityException("Salary cant be negative.");
			}
		}
	}
}
