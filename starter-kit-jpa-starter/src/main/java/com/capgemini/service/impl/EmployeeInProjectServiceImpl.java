package com.capgemini.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.JpaEmployeeInProjectDao;
import com.capgemini.exception.AddEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.EmployeeInProjectEntity;
import com.capgemini.service.EmployeeInProjectService;
import com.capgemini.service.ProjectManageService;


@Service
@Transactional
public class EmployeeInProjectServiceImpl implements EmployeeInProjectService {

	@Autowired
	private JpaEmployeeInProjectDao eipDao;

	@Autowired
	private ProjectManageService projectMenageService;

	@Override
	public List<EmployeeInProjectEntity> findAll() {
		return eipDao.findAll();
	}

	@Override
	public EmployeeInProjectEntity findById(Long id) {
		return eipDao.findOne(id);
	}

	@Override
	public void save(EmployeeInProjectEntity assignment) throws AddEntityException {
		try {
			projectMenageService.isAddingAssignmentPossible(assignment);
			eipDao.saveAndFlush(assignment);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityViolationException("Require value is null.");
		} catch (InvalidDataAccessApiUsageException e) {
			throw new InvalidDataAccessApiUsageException("Foreign key not in DB");
		}
	}

	@Override
	public List<EmployeeInProjectEntity> findEmployeeActualAssignmentsByEmployeeId(Long id) {
		return eipDao.findActualAssignmentsForEmployee(id);
	}

	@Override
	public List<EmployeeInProjectEntity> findActiveAssignmentInProject(Long projectId) {
		return eipDao.findActiveAssignmentsWithUnfinishedProjectsByProjectId(projectId);
	}

	@Override
	public void finishEmployeesAssignmentToProject(Long employeeId, Long projectId) throws WrongQueryException, AddEntityException {
		EmployeeInProjectEntity assignment = eipDao.findAssignmentByEmployeeIdAndProjectIdWithNullEndDate(employeeId,
				projectId);
		if (assignment == null) {
			throw new WrongQueryException("Not such employee in project whith unfinished assignment");
		}
		assignment.setEndDate(new Date());
		this.save(assignment);
	}

	@Override
	public List<EmployeeEntity> findEmployeesInProjectWorkedOverNumberOfMonths(Long projectId, int monthNumber) {
		Map<EmployeeEntity, Integer> tempEmployees = new HashMap<>();
		List<EmployeeInProjectEntity> allAssignment = eipDao.findAllAssignmentsByProjectID(projectId);
		if (allAssignment != null) {
			calculateMonthCountForEmployee(tempEmployees, allAssignment);
			return getEmplyeeWorkedOverMonthNumber(tempEmployees, monthNumber);
		}
		return new ArrayList<>();
	}

	private List<EmployeeEntity> getEmplyeeWorkedOverMonthNumber(Map<EmployeeEntity, Integer> employeesMap, int monthNumber) {
		List<EmployeeEntity> employees = new ArrayList<>();
		for (EmployeeEntity employee : employeesMap.keySet()) {
			if (employeesMap.get(employee) > monthNumber) {
				employees.add(employee);
			}
		}
		return employees;
	}

	private void calculateMonthCountForEmployee(Map<EmployeeEntity, Integer> employeesMap, List<EmployeeInProjectEntity> assignments) {
		for (EmployeeInProjectEntity assignment : assignments) {
			Date startDate = assignment.getStartDate();
			Date endDate = assignment.getEndDate();
			int monthDiff;
			if (endDate == null) {
				monthDiff = getMonthDiff(startDate, new Date());
			} else {
				monthDiff = getMonthDiff(startDate, endDate);
			}
			setMapValue(employeesMap, assignment, monthDiff);
		}
	}

	private void setMapValue(Map<EmployeeEntity, Integer> employeesMap, EmployeeInProjectEntity assignment, int monthDiff) {
		EmployeeEntity employee = assignment.getEmployee();
		if (!employeesMap.containsKey(employee)) {
			employeesMap.put(employee, monthDiff);
		} else {
			Integer value = employeesMap.get(employee);
			employeesMap.put(employee, value + monthDiff);
		}
	}

	@SuppressWarnings("deprecation")
	private int getMonthDiff(Date startDate, Date endDate) {
		int startDateMonthCount = startDate.getYear() * 12 + startDate.getMonth();
		int endDateMonthCount = endDate.getYear() * 12 + endDate.getMonth();
		return endDateMonthCount - startDateMonthCount + 1;
	}
}
