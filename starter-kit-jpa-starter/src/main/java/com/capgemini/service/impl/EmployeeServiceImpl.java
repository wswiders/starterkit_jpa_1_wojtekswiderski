package com.capgemini.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.JpaEmployeeDao;
import com.capgemini.exception.DeleteEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.ContactEntity;
import com.capgemini.model.entity.DepartmentEntity;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.service.EmployeeDeleteValidationSerice;
import com.capgemini.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private JpaEmployeeDao employeeDao;

	@Autowired
	private EmployeeDeleteValidationSerice deleteValidService;

	@Override
	public List<EmployeeEntity> findAll() {
		return employeeDao.findAll();
	}

	@Override
	public EmployeeEntity findById(Long employeeId) {
		return employeeDao.findOne(employeeId);
	}

	@Override
	public void save(EmployeeEntity employee) {
		try {
			employeeDao.saveAndFlush(employee);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityViolationException("Required value is null or name is not unique");
		} catch (InvalidDataAccessApiUsageException e) {
			throw new InvalidDataAccessApiUsageException("Foreign key not in DB");
		}
	}

	@Override
	public void delete(Long employeeId) throws DeleteEntityException, WrongQueryException {
		EmployeeEntity employee = this.findById(employeeId);
		if (employee == null) {
			throw new WrongQueryException("Employee not in data base");
		} else if (!deleteValidService.isDeletePosible(employeeId)) {
			throw new DeleteEntityException("Employee has unfinished projects");
		}
		employeeDao.delete(employee);
	}

	@Override
	public List<EmployeeEntity> findByNameAndSurname(String name, String surname) {
		return employeeDao.findByNameContainingAndSurnameContaining(name, surname);
	}

	@Override
	public List<EmployeeEntity> findByDepartment(Long employeeId) {
		return employeeDao.findByDepartmentId(employeeId);
	}

	@Override
	public void updateData(Long employeeId, EmployeeEntity updatedEntity) throws WrongQueryException {
		EmployeeEntity employee = this.findById(employeeId);
		if (employee != null) {
			updateBirthDade(employee, updatedEntity.getBirthDate());
			updateDepartment(employee, updatedEntity.getDepartment());
			updateName(employee, updatedEntity.getName());
			updatePesel(employee, updatedEntity.getPesel());
			updateSurname(employee, updatedEntity.getSurname());
			if (updatedEntity.getContact() != null) {
				String newEmail = updatedEntity.getContact().getEmail();
				String newPhone = updatedEntity.getContact().getPhone();
				String newHomePhone = updatedEntity.getContact().getHomePhone();
				updateContact(employee.getContact(), newEmail, newPhone, newHomePhone);
			}
			this.save(employee);
		} else {
			throw new WrongQueryException("Employee not in data base");
		}
	}

	@Override
	public void setNullDepartment(Long employeeId) {
		EmployeeEntity employee = this.findById(employeeId);
		employee.setDepartment(null);
		this.save(employee);
	}

	private void updateContact(ContactEntity contact, String email, String phone, String homePhone) {
		contact.setHomePhone(homePhone);
		contact.setPhone(phone);
		contact.setEmail(email);
	}

	private void updateName(EmployeeEntity e, String name) {
		if (name != null) {
			e.setName(name);
		}
	}

	private void updateBirthDade(EmployeeEntity e, Date date) {
		if (date != null) {
			e.setBirthDate(date);
		}
	}

	private void updateSurname(EmployeeEntity e, String surname) {
		if (surname != null) {
			e.setSurname(surname);
		}
	}

	private void updateDepartment(EmployeeEntity e, DepartmentEntity department) {
		if (department != null) {
			e.setDepartment(department);
		}
	}

	private void updatePesel(EmployeeEntity e, String pesel) {
		if (pesel != null) {
			e.setPesel(pesel);
		}
	}
}
