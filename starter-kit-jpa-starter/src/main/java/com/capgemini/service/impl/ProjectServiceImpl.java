package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.JpaProjectDao;
import com.capgemini.exception.DeleteEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.model.enumerator.ProjectType;
import com.capgemini.service.ProjectManageService;
import com.capgemini.service.ProjectService;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private JpaProjectDao projectDao;

	@Autowired
	private ProjectManageService projectMenageService;

	@Override
	public List<ProjectEntity> findAll() {
		return projectDao.findAll();
	}

	@Override
	public ProjectEntity findById(Long id) {
		return projectDao.findOne(id);
	}

	@Override
	public void save(ProjectEntity project) {
		try {
			projectDao.saveAndFlush(project);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityViolationException("Required value is null or name is not unique");
		}
	}

	@Override
	public void delete(Long id) throws DeleteEntityException, WrongQueryException {
		ProjectEntity project = this.findById(id);
		if (project == null) {
			throw new WrongQueryException("Employee not in data base");
		}
		if (!projectMenageService.isDeletePossible(id)) {
			throw new DeleteEntityException("Project has unfinished assignment");
		}
		projectDao.delete(project);
	}

	@Override
	public void updateData(Long id, ProjectEntity updatedProject) throws WrongQueryException {
		ProjectEntity project = this.findById(id);
		if (project != null) {
			updateName(project, updatedProject.getName());
			updateType(project, updatedProject.getType());
			this.save(project);
		} else {
			throw new WrongQueryException("Project not in data base");
		}
	}

	private void updateType(ProjectEntity project, ProjectType type) {
		if (type != null) {
			project.setType(type);
		}
	}

	private void updateName(ProjectEntity project, String name) {
		if (name != null) {
			project.setName(name);
		}
	}
}
