package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.model.entity.EmployeeInProjectEntity;
import com.capgemini.service.EmployeeDeleteValidationSerice;
import com.capgemini.service.EmployeeInProjectService;

@Service
@Transactional
public class EmployeeDeleteValidationSericeImpl implements EmployeeDeleteValidationSerice {

	@Autowired
	private EmployeeInProjectService employeeInProjectService;

	@Override
	public boolean isDeletePosible(Long employeeId) {
		List<EmployeeInProjectEntity> actualProjects = employeeInProjectService
				.findEmployeeActualAssignmentsByEmployeeId(employeeId);
		if (actualProjects.size() == 0) {
			return true;
		}
		return false;
	}
}
