package com.capgemini.service;

public interface EmployeeDeleteValidationSerice {

	public boolean isDeletePosible(Long employeeId);
}
