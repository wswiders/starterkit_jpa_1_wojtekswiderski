package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.AddEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.EmployeeInProjectEntity;

public interface EmployeeInProjectService {

	public List<EmployeeInProjectEntity> findAll();

	public EmployeeInProjectEntity findById(Long id);

	public void save(EmployeeInProjectEntity assignment) throws AddEntityException;

	public List<EmployeeInProjectEntity> findEmployeeActualAssignmentsByEmployeeId(Long id);

	public List<EmployeeInProjectEntity> findActiveAssignmentInProject(Long projectId);

	public void finishEmployeesAssignmentToProject(Long employeeId, Long projectId) throws WrongQueryException, AddEntityException;

	public List<EmployeeEntity> findEmployeesInProjectWorkedOverNumberOfMonths(Long projectId, int monthNumber);
}
