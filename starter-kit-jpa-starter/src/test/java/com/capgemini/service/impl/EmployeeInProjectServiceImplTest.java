package com.capgemini.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.exception.AddEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.EmployeeInProjectEntity;
import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.model.enumerator.EmployeeRole;
import com.capgemini.service.EmployeeInProjectService;
import com.capgemini.service.EmployeeService;
import com.capgemini.service.ProjectService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeInProjectServiceImplTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Autowired
	private EmployeeInProjectService employeeInProjectService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeeService employeeService;

	@Test
	@Transactional
	public void shouldReturnAllProjects() {
		// when
		List<EmployeeInProjectEntity> projects = employeeInProjectService.findAll();
		// then
		int actual = projects.size();
		assertEquals(17, actual);
	}

	@Test
	@Transactional
	public void shouldFindAssignmentById() throws WrongQueryException {
		// when
		EmployeeInProjectEntity assignment = employeeInProjectService.findById(1L);
		// then
		EmployeeRole actual = assignment.getRole();
		assertEquals(EmployeeRole.PL, actual);
	}

	@Test
	@Transactional
	public void shouldNotFindAssignmentById() throws WrongQueryException {
		// when
		EmployeeInProjectEntity assignment = employeeInProjectService.findById(20L);
		// then
		assertNull(assignment);
	}

	@Test
	@Transactional
	public void shouldAddAssignment() throws WrongQueryException, AddEntityException {
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(2L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.DEV);
		assignment.setStartDate(new Date());
		assignment.setSalary(new BigDecimal(123.54));
		// when
		employeeInProjectService.save(assignment);
		// then
		int actual = employeeInProjectService.findActiveAssignmentInProject(2L).size();
		assertEquals(4, actual);
	}

	@Test
	@Transactional
	public void shouldNotAddAssignmentWhenOtherManagerInProject() throws WrongQueryException, AddEntityException {
		// expected
		thrown.expect(AddEntityException.class);
		thrown.expectMessage("In project can be only one PL.");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(2L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.PL);
		assignment.setStartDate(new Date());
		assignment.setSalary(new BigDecimal(123.54));
		// when
		employeeInProjectService.save(assignment);
	}

	@Test
	@Transactional
	public void shouldFinishManagerWorkAndAddNew() throws WrongQueryException, AddEntityException {
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(1L));
		assignment.setEmployee(employeeService.findById(11L));
		assignment.setRole(EmployeeRole.PL);
		assignment.setStartDate(new Date());
		assignment.setSalary(new BigDecimal(123.54));
		// when
		employeeInProjectService.finishEmployeesAssignmentToProject(1L, 1L);
		employeeInProjectService.save(assignment);
		// then
		int actual = employeeInProjectService.findEmployeeActualAssignmentsByEmployeeId(11L).size();
		assertEquals(1, actual);
	}

	@Test
	@Transactional
	public void shouldAddAssignmentAndAllowToQuitIt() throws WrongQueryException, AddEntityException {
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(2L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.DEV);
		assignment.setStartDate(new Date());
		assignment.setSalary(new BigDecimal(123.54));
		// when
		employeeInProjectService.save(assignment);
		employeeInProjectService.finishEmployeesAssignmentToProject(4L, 2L);
		// then
		Date actual = employeeInProjectService.findById(18L).getEndDate();
		assertNotNull(actual);
	}

	@Test
	@Transactional
	public void shouldNotAddAssignmentWhenForeignKeyNotInDataBase() throws WrongQueryException, AddEntityException {
		// expected
		thrown.expect(InvalidDataAccessApiUsageException.class);
		thrown.expectMessage("Foreign key not in DB");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(new ProjectEntity());
		assignment.setEmployee(new EmployeeEntity());
		// when
		employeeInProjectService.save(assignment);
	}

	@Test
	@Transactional
	public void shouldNotAddAssignmentWhenRequireValueIsEmpty() throws WrongQueryException, AddEntityException {
		// expected
		thrown.expect(DataIntegrityViolationException.class);
		thrown.expectMessage("Require value is null.");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(2L));
		assignment.setEmployee(employeeService.findById(4L));
		// when
		employeeInProjectService.save(assignment);
	}

	@Test
	@Transactional
	public void shouldReturnEmptyListWhenAllAsignmentsAreFinished() {
		// when
		List<EmployeeInProjectEntity> activeAsignments = employeeInProjectService
				.findEmployeeActualAssignmentsByEmployeeId(11L);
		// then
		int actual = activeAsignments.size();
		assertEquals(0, actual);
	}

	@Test
	@Transactional
	public void shouldReturnListOfActualAsignments() {
		// when
		List<EmployeeInProjectEntity> activeAsignments = employeeInProjectService
				.findEmployeeActualAssignmentsByEmployeeId(1L);
		// then
		int actual = activeAsignments.size();
		assertEquals(2, actual);
	}

	@Test
	@Transactional
	public void shouldReturnListOfActualAssignmentOfUnfinishedProject() {
		// when
		List<EmployeeInProjectEntity> activeAssignments = employeeInProjectService.findActiveAssignmentInProject(1L);
		// then
		int actual = activeAssignments.size();
		assertEquals(10, actual);
	}

	@Test
	@Transactional
	public void shouldReturnEmptyListOfActualAssignmentOfFinishedProject() {
		// when
		List<EmployeeInProjectEntity> activeAssignments = employeeInProjectService.findActiveAssignmentInProject(3L);
		// then
		int actual = activeAssignments.size();
		assertEquals(0, actual);
	}

	@Test
	@Transactional
	public void shouldFinishEmployeesInProjectWork() throws WrongQueryException, AddEntityException {
		// when
		employeeInProjectService.finishEmployeesAssignmentToProject(1L, 1L);
		// then
		Date actual = employeeInProjectService.findById(1L).getEndDate();
		assertNotNull(actual);
	}

	@Test
	@Transactional
	public void shouldThrowExceptionWhenEmployeeNotInProject() throws WrongQueryException, AddEntityException {
		// expected
		thrown.expect(WrongQueryException.class);
		thrown.expectMessage("Not such employee in project whith unfinished assignment");
		// when
		employeeInProjectService.finishEmployeesAssignmentToProject(7L, 3L);
	}

	@Test
	@Transactional
	public void shouldReturnAllEmployessThatWorkedOver30Month() {
		// when
		List<EmployeeEntity> employees = employeeInProjectService.findEmployeesInProjectWorkedOverNumberOfMonths(2L,
				30);
		// then
		Long actual = employees.get(0).getId();
		assertEquals(new Long(2), actual);
	}

	@Test
	@Transactional
	public void shouldReturnAllEmployessThatWorkedOver25MonthInFirstProject() {
		// when
		List<EmployeeEntity> employees = employeeInProjectService.findEmployeesInProjectWorkedOverNumberOfMonths(1L,
				25);
		// then
		int actual = employees.size();
		assertEquals(10, actual);
	}
}