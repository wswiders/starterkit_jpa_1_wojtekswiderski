package com.capgemini.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.exception.DeleteEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.ContactEntity;
import com.capgemini.model.entity.DepartmentEntity;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.service.DepartmentService;
import com.capgemini.service.EmployeeInProjectService;
import com.capgemini.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceImplTest {

	private static final int START_NUMBER_EMPLOYEES = 11;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private EmployeeInProjectService employeeInProjectService;

	@Autowired
	private DepartmentService departmentService;

	@Test
	@Transactional
	public void shouldReturnAllEmployee() {
		// when
		List<EmployeeEntity> employees = employeeService.findAll();
		// then
		int actual = employees.size();
		assertEquals(11L, actual);
	}

	@Test
	@Transactional
	public void shouldFindEmployeeById() {
		// when
		EmployeeEntity employee = employeeService.findById(1L);
		// then
		String actual = employee.getName();
		assertEquals("Jina", actual);
	}

	@Test
	@Transactional
	public void shouldNotFindEmployeeById() {
		// when
		EmployeeEntity employee = employeeService.findById(12L);
		// then
		assertNull(employee);
	}

	@Test
	@Transactional
	public void shouldDeleteEmployee() throws DeleteEntityException, WrongQueryException {
		// given
		int expected = START_NUMBER_EMPLOYEES - 1;
		// when
		employeeService.delete(11L);
		// then
		int actual = employeeService.findAll().size();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldDeleteAllDataAboutEmployeesProjects() throws DeleteEntityException, WrongQueryException {
		// when
		employeeService.delete(11L);
		// then
		int actual = employeeInProjectService.findAll().size();
		assertEquals(15, actual);
	}

	@Test
	@Transactional
	public void shouldNotDeleteEmployeeWhenEmployeeDontExist() throws DeleteEntityException, WrongQueryException {
		// expected
		thrown.expect(WrongQueryException.class);
		thrown.expectMessage("Employee not in data base");
		// when
		employeeService.delete(12L);
	}

	@Test
	@Transactional
	public void shouldNotDeleteEmployeeWithUnfinishedProjects() throws WrongQueryException, DeleteEntityException {
		// expected
		thrown.expect(DeleteEntityException.class);
		thrown.expectMessage("Employee has unfinished projects");
		// when
		employeeService.delete(1L);
	}

	@Test
	@Transactional
	public void shouldNotFindEmployeesByNameAndSurname() {
		// when
		List<EmployeeEntity> employee = employeeService.findByNameAndSurname("Andrzej", "Strzelba");
		// then
		int actual = employee.size();
		assertEquals(0, actual);
	}

	@Test
	@Transactional
	public void shouldReturnEmployeeByNameAndSurname() {
		// when
		List<EmployeeEntity> employees = employeeService.findByNameAndSurname("Jina", "Barański");
		// then
		int actual = employees.size();
		assertEquals(1, actual);
	}

	@Test
	@Transactional
	public void shouldReturnAllEmployeesByNameAndSurname() {
		// when
		List<EmployeeEntity> employees = employeeService.findByNameAndSurname("a", "ski");
		// then
		int actual = employees.size();
		assertEquals(3, actual);
	}

	@Test
	@Transactional
	public void shouldReturnAllEmployeesByDepartment() {
		// when
		List<EmployeeEntity> employees = employeeService.findByDepartment(1L);
		// then
		int actual = employees.size();
		assertEquals(3, actual);
	}

	@Test
	@Transactional
	public void shouldReturnEmptyListWhenDepartmentDontExist() {
		// when
		List<EmployeeEntity> employees = employeeService.findByDepartment(5L);
		// then
		int actual = employees.size();
		assertEquals(0, actual);
	}

	@Test
	@Transactional
	public void shouldUpdateName() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		String expected = "Andrzej";
		updatedEmployee.setName(expected);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		String actual = employeeService.findById(1L).getName();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldUpdateOnlyChangedData() throws WrongQueryException {
		// given
		String expected = employeeService.findById(1L).getSurname();
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		updatedEmployee.setName("Andrzej");
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		String actual = employeeService.findById(1L).getSurname();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldChangeVersionAfterUpdate() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		updatedEmployee.setName("Andrzej");
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		Long actual = employeeService.findById(1L).getVersion();
		assertEquals(new Long(1), actual);
	}

	@Test
	@Transactional
	public void shouldChangeLastUpdateDate() throws WrongQueryException {
		// given
		Date before = employeeService.findById(1L).getUpdateDate();
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		updatedEmployee.setName("Andrzej");
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		Date actual = employeeService.findById(1L).getUpdateDate();
		assertFalse(Math.abs(actual.getTime() - before.getTime()) < 1000);
	}

	@Test
	@Transactional
	public void shouldUpdateSurname() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		String expected = "Strzelba";
		updatedEmployee.setSurname(expected);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		String actual = employeeService.findById(1L).getSurname();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldUpdatePesel() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		String expected = "99999999999";
		updatedEmployee.setPesel(expected);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		String actual = employeeService.findById(1L).getPesel();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldUpdateBirthDate() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		Date expected = new Date();
		updatedEmployee.setBirthDate(expected);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		Date actual = employeeService.findById(1L).getBirthDate();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldUpdateDepartment() throws WrongQueryException {
		// given
		DepartmentEntity expected = departmentService.findById(3L);
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		updatedEmployee.setDepartment(expected);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		DepartmentEntity actual = employeeService.findById(1L).getDepartment();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldUpdateEmail() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		ContactEntity contact = new ContactEntity();
		String expected = "mail_new@test.pl";
		contact.setEmail(expected);
		updatedEmployee.setContact(contact);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		String actual = employeeService.findById(1L).getContact().getEmail();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldUpdatePhone() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		ContactEntity contact = new ContactEntity();
		String expected = "723062378";
		contact.setPhone(expected);
		updatedEmployee.setContact(contact);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		String actual = employeeService.findById(1L).getContact().getPhone();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldUpdateHomePhone() throws WrongQueryException {
		// given
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		ContactEntity contact = new ContactEntity();
		String expected = "00723062378";
		contact.setHomePhone(expected);
		updatedEmployee.setContact(contact);
		// when
		employeeService.updateData(1L, updatedEmployee);
		// then
		String actual = employeeService.findById(1L).getContact().getHomePhone();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldNotUpdateEmployeeDataWhenEmployeeNotInDB() throws WrongQueryException {
		// expected
		thrown.expect(WrongQueryException.class);
		thrown.expectMessage("Employee not in data base");
		EmployeeEntity updatedEmployee = new EmployeeEntity();
		// when
		employeeService.updateData(20L, updatedEmployee);
	}

	@Test
	@Transactional
	public void shouldSetNullDepartment() {
		// when
		employeeService.setNullDepartment(1L);
		// then
		DepartmentEntity actual = employeeService.findById(1L).getDepartment();
		assertEquals(null, actual);
	}

	@Test
	@Transactional
	public void shouldAddNewEmployee() {
		// given
		EmployeeEntity newEmployee = new EmployeeEntity();
		newEmployee.setName("Jan");
		newEmployee.setSurname("Kowalski");
		newEmployee.setBirthDate(new Date());
		// when
		employeeService.save(newEmployee);
		// then
		int actual = employeeService.findAll().size();
		assertEquals(12, actual);
	}

	@Test
	@Transactional
	public void shouldNotAddNewEmployeeWhenRequirePoleIsEmpty() {
		// expected
		thrown.expect(DataIntegrityViolationException.class);
		thrown.expectMessage("Required value is null");
		// given
		EmployeeEntity newEmployee = new EmployeeEntity();
		newEmployee.setName("Jan");
		newEmployee.setBirthDate(new Date());
		// when
		employeeService.save(newEmployee);
	}

	@Test
	@Transactional
	public void shouldNotAddNewEmployeeWhenForeignKeyNotInDB() {
		// expected
		thrown.expect(InvalidDataAccessApiUsageException.class);
		thrown.expectMessage("Foreign key not in DB");
		// given
		EmployeeEntity newEmployee = new EmployeeEntity();
		newEmployee.setName("Jan");
		newEmployee.setSurname("Kowalski");
		newEmployee.setBirthDate(new Date());
		newEmployee.setDepartment(new DepartmentEntity());
		// when
		employeeService.save(newEmployee);
	}

	@Test
	@Transactional
	public void shouldOptimisticLockThrowException() {
		// expected
		thrown.expect(ObjectOptimisticLockingFailureException.class);
		// given
		EmployeeEntity e1 = employeeService.findById(1L);
		EmployeeEntity e2 = employeeService.findById(1l);
		// when
		em.detach(e1);
		e1.setName("dsadsad");
		employeeService.save(e1);
		e2.setName("dadsadsa");
		employeeService.save(e2);
	}
}
