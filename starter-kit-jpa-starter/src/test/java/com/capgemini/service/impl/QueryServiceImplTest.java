package com.capgemini.service.impl;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.criteriaobject.EmployeeSearchCriteria;
import com.capgemini.model.entity.EmployeeEntity;
import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.model.enumerator.EmployeeRole;
import com.capgemini.service.ProjectService;
import com.capgemini.service.QueryService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QueryServiceImplTest {

	@Autowired
	private QueryService queryService;

	@Autowired
	private ProjectService projectService;

	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	@Test
	@Transactional
	public void shouldFindEmployeesByCriteria() throws ParseException {
		// given
		EmployeeSearchCriteria criteria = new EmployeeSearchCriteria();
		criteria.setProjectId(1L);
		criteria.setRole(EmployeeRole.DEV);
		Date firstDate = format.parse("2014-07-01");
		criteria.setFirstDate(firstDate);
		Date secondDate = format.parse("2015-07-01");
		criteria.setSecondDate(secondDate);
		// when
		List<EmployeeEntity> resoults = queryService.findEmployeesByCriteria(criteria);
		// then
		int actual = resoults.size();
		assertEquals(4, actual);
	}
	
	@Test
	@Transactional
	public void shouldDontFindEmployeesByCriteria() throws ParseException {
		// given
		EmployeeSearchCriteria criteria = new EmployeeSearchCriteria();
		Date firstDate = format.parse("2012-12-01");
		criteria.setFirstDate(firstDate);
		Date secondDate = format.parse("2013-07-01");
		criteria.setSecondDate(secondDate);
		// when
		List<EmployeeEntity> resoults = queryService.findEmployeesByCriteria(criteria);
		// then
		int actual = resoults.size();
		assertEquals(0, actual);
	}
	
	@Test
	@Transactional
	public void shouldReturnCountOfProjectsWithOver10EmployeesInSpecificDate() throws ParseException {
		// given
		Date date = format.parse("2014-07-01");
		// when
		int actual = queryService.countProjectWithOver10Employees(date);
		// then
		assertEquals(1, actual);
	}

	@Test
	@Transactional
	public void shouldEmptyListOfProjectWithOver10EmployeesInSpecificDate() throws ParseException {
		// given
		Date date = format.parse("2016-07-01");
		// when
		int actual = queryService.countProjectWithOver10Employees(date);
		// then
		assertEquals(0, actual);
	}

	@Test
	@Transactional
	public void shouldReturnProjectsWithOver10EmployeesInSpecificDate() throws ParseException, WrongQueryException {
		// given
		Date date = format.parse("2014-07-01");
		Long expected = projectService.findById(1L).getId();
		// when
		List<ProjectEntity> projects = queryService.findProjectWithOver10Employees(date);
		// then
		int actualSize = projects.size();
		Long actual = projects.get(0).getId();
		assertEquals(1, actualSize);
		assertEquals(expected, actual);
	}
}
