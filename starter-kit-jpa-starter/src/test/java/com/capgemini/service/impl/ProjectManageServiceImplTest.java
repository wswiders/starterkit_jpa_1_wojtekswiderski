package com.capgemini.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.exception.AddEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.EmployeeInProjectEntity;
import com.capgemini.model.enumerator.EmployeeRole;
import com.capgemini.service.EmployeeService;
import com.capgemini.service.ProjectManageService;
import com.capgemini.service.ProjectService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectManageServiceImplTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Autowired
	private ProjectManageService projectMenageService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeeService employeeService;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Test
	@Transactional
	public void shouldReturnTrueWhenProjectIsFinished() {
		// when
		boolean actual = projectMenageService.isDeletePossible(3L);
		// then
		assertTrue(actual);
	}

	@Test
	@Transactional
	public void shouldReturnFalseWhenProjectIsUnfinished() {
		// when
		boolean actual = projectMenageService.isDeletePossible(1L);
		// then
		assertFalse(actual);
	}

	@Test
	@Transactional
	public void shouldThrowExceptionWhenTryToAddSecondManagerToProject() throws AddEntityException, WrongQueryException {
		// expected
		thrown.expect(AddEntityException.class);
		thrown.expectMessage("In project can be only one PL.");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(1L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.PL);
		// when
		projectMenageService.isAddingAssignmentPossible(assignment);
	}

	@Test
	@Transactional
	public void shouldThrowExceptionWhenEmployeeAlreadyInProject() throws AddEntityException, WrongQueryException {
		// expected
		thrown.expect(AddEntityException.class);
		thrown.expectMessage("Employee already had role in project.");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(1L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.DEV);
		// when
		projectMenageService.isAddingAssignmentPossible(assignment);
	}

	@Test
	@Transactional
	public void shouldThrowExceptionWhenStartAssignmentDateBeforeProjectCreationTime()
			throws AddEntityException, WrongQueryException, ParseException {
		// expected
		thrown.expect(AddEntityException.class);
		thrown.expectMessage("Project start date must be after project creation date.");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(2L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.DEV);
		Date startDate = dateFormat.parse("2013-01-01");
		assignment.setStartDate(startDate);
		// when
		projectMenageService.isAddingAssignmentPossible(assignment);
	}

	@Test
	@Transactional
	public void shouldThrowExceptionWhenEndAssignmentDateBeforeStartDate()
			throws AddEntityException, WrongQueryException, ParseException {
		// expected
		thrown.expect(AddEntityException.class);
		thrown.expectMessage("Project end date must be after project start date.");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(2L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.DEV);
		Date startDate = dateFormat.parse("2015-05-01");
		assignment.setStartDate(startDate);
		Date endDate = dateFormat.parse("2013-05-01");
		assignment.setEndDate(endDate);
		// when
		projectMenageService.isAddingAssignmentPossible(assignment);
	}

	@Test
	@Transactional
	public void shouldThrowExceptionWhenSalaryIsNegative()
			throws AddEntityException, WrongQueryException, ParseException {
		// expected
		thrown.expect(AddEntityException.class);
		thrown.expectMessage("Salary cant be negative.");
		// given
		EmployeeInProjectEntity assignment = new EmployeeInProjectEntity();
		assignment.setProject(projectService.findById(2L));
		assignment.setEmployee(employeeService.findById(4L));
		assignment.setRole(EmployeeRole.DEV);
		Date startDate = dateFormat.parse("2015-05-01");
		assignment.setStartDate(startDate);
		assignment.setSalary(new BigDecimal(-123.54));
		// then
		projectMenageService.isAddingAssignmentPossible(assignment);
	}
}
