package com.capgemini.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.service.EmployeeDeleteValidationSerice;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeDeleteValidationSericeImplTest {

	@Autowired
	private EmployeeDeleteValidationSerice employeeDeleteValidService;

	@Test
	@Transactional
	public void shouldReturnTrueWhenEmployeeFinishedAllProjects() {
		//when
		boolean actual = employeeDeleteValidService.isDeletePosible(11L);
		//then
		assertTrue(actual);
	}

	@Test
	@Transactional
	public void shouldReturnFalseWhenEmployeeDontFinishedAllProjects() {
		//when
		boolean actual = employeeDeleteValidService.isDeletePosible(1L);
		//then
		assertFalse(actual);
	}
}
