package com.capgemini.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.model.entity.DepartmentEntity;
import com.capgemini.service.DepartmentService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DepartmentServiceImplTest {
	
	@Autowired
	private DepartmentService departmentService;

	@Test
	@Transactional
	public void shouldFindDepartmentById() {
		//when
		DepartmentEntity departament = departmentService.findById(1L);
		//then
		assertNotNull(departament);
	}

	@Test
	@Transactional
	public void shouldNotFindDepartmentById() {
		//when
		DepartmentEntity departament = departmentService.findById(20L);
		//then
		assertNull(departament);
	}
}
