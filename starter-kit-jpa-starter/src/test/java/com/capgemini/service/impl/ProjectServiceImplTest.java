package com.capgemini.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.exception.DeleteEntityException;
import com.capgemini.exception.WrongQueryException;
import com.capgemini.model.entity.ProjectEntity;
import com.capgemini.model.enumerator.ProjectType;
import com.capgemini.service.EmployeeInProjectService;
import com.capgemini.service.ProjectService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectServiceImplTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeeInProjectService employeeInProjectService;

	@Test
	@Transactional
	public void shouldReturnAllProjects() {
		// when
		List<ProjectEntity> projects = projectService.findAll();
		// then
		int actual = projects.size();
		assertEquals(3, actual);
	}

	@Test
	@Transactional
	public void shouldFindProjectById() throws WrongQueryException {
		// when
		ProjectEntity project = projectService.findById(1L);
		// then
		String actual = project.getName();
		assertEquals("Quality", actual);
	}

	@Test
	@Transactional
	public void shouldNotFindProjectById() throws WrongQueryException {
		// when
		ProjectEntity project = projectService.findById(4L);
		// then
		assertNull(project);
	}

	@Test
	@Transactional
	public void shouldAddNewProject() {
		// given
		ProjectEntity project = new ProjectEntity();
		project.setName("newProjects");
		project.setType(ProjectType.INTERNAL);
		// when
		projectService.save(project);
		// then
		int actual = projectService.findAll().size();
		assertEquals(4, actual);
	}

	@Test
	@Transactional
	public void shouldNotAddProjectWhenNameIsNotUnique() {
		// expected
		thrown.expect(DataIntegrityViolationException.class);
		thrown.expectMessage("Required value is null or name is not unique");
		// given
		ProjectEntity project = new ProjectEntity();
		project.setName("Quality");
		project.setType(ProjectType.INTERNAL);
		// when
		projectService.save(project);
	}

	@Test
	@Transactional
	public void shouldNotAddProjectWhenRequiredParamIsEmpty() {
		// expected
		thrown.expect(DataIntegrityViolationException.class);
		thrown.expectMessage("Required value is null or name is not unique");
		// given
		ProjectEntity project = new ProjectEntity();
		project.setType(ProjectType.INTERNAL);
		// when
		projectService.save(project);
	}

	@Test
	@Transactional
	public void shouldDeleteProjectWhenAllAsignmentFinished() throws DeleteEntityException, WrongQueryException {
		// when
		projectService.delete(3l);
		// then
		int actual = projectService.findAll().size();
		assertEquals(2, actual);
	}

	@Test
	@Transactional
	public void shouldDeleteProjectAndAllAsignment() throws WrongQueryException, DeleteEntityException {
		// when
		projectService.delete(3l);
		// then
		int actual = employeeInProjectService.findAll().size();
		assertEquals(16, actual);
	}

	@Test
	@Transactional
	public void shouldNotDeleteProjectWithUnfinishedAssignment() throws WrongQueryException, DeleteEntityException {
		// expected
		thrown.expect(DeleteEntityException.class);
		thrown.expectMessage("Project has unfinished assignment");
		// when
		projectService.delete(1l);
	}
	
	@Test
	@Transactional
	public void shouldNotDeleteProjectWhenProjectDontExist() throws WrongQueryException, DeleteEntityException {
		// expected
		thrown.expect(WrongQueryException.class);
		thrown.expectMessage("Employee not in data base");
		// when
		projectService.delete(7l);
	}

	@Test
	@Transactional
	public void shouldUpdateProjectName() throws WrongQueryException {
		// given
		ProjectEntity updatedProject = new ProjectEntity();
		String expected = "new_name";
		updatedProject.setName(expected);
		// when
		projectService.updateData(1L, updatedProject);
		// then
		String actual = projectService.findById(1L).getName();
		assertEquals(expected, actual);
	}

	@Test
	@Transactional
	public void shouldNotUpdateProjectNameWhenNotUnique() throws WrongQueryException {
		// expected
		thrown.expect(DataIntegrityViolationException.class);
		thrown.expectMessage("Required value is null or name is not unique");
		// given
		ProjectEntity updatedProject = new ProjectEntity();
		String expected = "Quality";
		updatedProject.setName(expected);
		// when
		projectService.updateData(2L, updatedProject);
	}

	@Test
	@Transactional
	public void shouldUpdateProjectType() throws WrongQueryException {
		// given
		ProjectEntity updatedProject = new ProjectEntity();
		updatedProject.setType(ProjectType.INTERNAL);
		// when
		projectService.updateData(1L, updatedProject);
		// then
		ProjectType actual = projectService.findById(1L).getType();
		assertEquals(ProjectType.INTERNAL, actual);
	}

	@Test
	@Transactional
	public void shouldNotUpdateProjectWhenProjectNotInDB() throws WrongQueryException {
		// expected
		thrown.expect(WrongQueryException.class);
		thrown.expectMessage("Project not in data base");
		// given
		ProjectEntity updatedProject = new ProjectEntity();
		updatedProject.setType(ProjectType.INTERNAL);
		// when
		projectService.updateData(7L, updatedProject);
	}
}
